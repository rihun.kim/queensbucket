import gochang from './gochang.png';
import hongcheon from './hongcheon.png';
import jecheon from './jecheon.png';

const getAllMaps = () => {
  return [
      {
        "name": "gochang",
        "src": gochang,
      },
      {
        "name": "hongcheon",
        "src": hongcheon,
      },
      {
        "name": "jecheon",
        "src": jecheon,
      },
    ];
}

export const getMap = (_keyword) => {
  const keyword = _keyword.toLowerCase();
  return  getAllMaps().filter(map => map.name === keyword ? true : false)[0].src;
}

