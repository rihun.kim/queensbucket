import gochang from './gochang2.jpg';
import hongcheon from './hongcheon1.jpg';
import jecheon from './jecheon1.jpg';

const getAllScreens = () => {
  return [
      {
        "name": "gochang",
        "src": gochang,
      },
      {
        "name": "hongcheon",
        "src": hongcheon,
      },
      {
        "name": "jecheon",
        "src": jecheon,
      }
    ];
}

export const getScreen = (_keyword) => {
  const keyword = _keyword.toLowerCase();
  return  getAllScreens().filter(screen => screen.name === keyword ? true : false)[0].src;
}