import productList from './Data.json';

const searchByName = (_product, _keyword) => {
  return _product.name.indexOf(_keyword) !== -1;
}

const searchByEName = (_product, _keyword) => {
  return _product.ename.toLowerCase().indexOf(_keyword) !== -1;
}

const searchByCode = (_product, _keyword) => {
  return _product.code.indexOf(_keyword) !== -1;
}

const searchByDate = (_product, _keyword) => {
  return _product.mdate.indexOf(_keyword) !== -1;
}

export const getAllProducts = () => {
  return productList;
}

export const getProducts = (_keyword) => {
  const keyword = _keyword.toLowerCase();
  return getAllProducts().filter(product => {
    return (searchByName(product, keyword) || searchByEName(product, keyword) || searchByCode(product, keyword) || searchByDate(product, keyword));
  });
}