import React, { Component } from 'react';

import { Header, MainLogo, SearchBox, SearchBoxInput } from './components/Header';
import CardContainer from './components/CardContainer';
import GlobalStyler from './components/GlobalStyler';
import Guider from './components/Guider';
import Footer from './components/Footer';

import { getProducts } from './databases/DataApi.js';

class App extends Component {
  state = {
    keyword: ""
  }

  searchBoxOnChange = event => {
    this.setState({ keyword: event.target.value })
  }

  render() {
    const { keyword } = this.state;
    const products = getProducts(keyword);

    return (
      <div>
        <GlobalStyler />
        <Header>
          <MainLogo />
          <SearchBox>
            <SearchBoxInput onChange={this.searchBoxOnChange} />
          </SearchBox>
        </Header>
        <CardContainer products={products} />
        <Guider />
        <Footer />
      </div>
    );
  }
};

export default App;