import React, { useState } from 'react';

import styled from "styled-components";

import search from '../resources/search.png';
import qrcode from '../resources/qrcode.png';

const GuideButton = styled.button`
  width: 56px;  
  height: 56px;
  
  background-color: #FFFFFFAD;
  border: none;
  border-radius: 50%;
  bottom: 10px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  cursor: pointer;
  color: #757575;
  font-size: 18px;
  font-weight: bold;
  outline: none;
  padding: 0;
  position: fixed;
  right: 10px;
  z-index: 4;

  @media (min-width : 530px) {
    bottom: 40px;
    right: 40px;
  }
`;

const GuideModal = styled.div`
  width: 280px;
  height: 230px;
  
  background-color: #FFFFFF;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
  border-radius: 5px;
  display: block;
  left: 0;
  margin: auto;
  opacity: 1;
  position: fixed;
  padding: 15px 20px 10px 20px;
  right: 0;
  top: 150px;
  z-index: 3;

  @media (min-width : 530px) {
    width: 310px;

    padding: 20px;
    top: 250px;
  }

  @media (min-width : 790px) {
    width: 700px;
    height: 170px;

    display: flex;
  }
`;

const MainSection = styled.div`
  display: flex;
  margin: 0 auto 0 auto;
  
  &:last-child {
    margin: 30px auto 0 auto;
    
    @media (min-width : 790px) {
      margin: 0 auto 0 auto;
    }
  }
`;

const SubSection = styled.div`
  margin: auto 0 auto 0;
`;

const Title = styled.div`
  color: #000000; 
  font-size: 13px;
  font-weight: 450;
  margin: 0 0 0 15px;

  @media (min-width : 530px) {
    margin: 0 0 0 30px;
  }

  @media (min-width : 790px) {
    font-size: 14px;
  }

  @media (min-width : 1050px) {
    font-size: 15px;
  }

`;

const Text = styled.div`
  color: #000000; 
  font-size: 13px;
  font-weight: 250;
  margin: 10px 0 0 15px;

  @media (min-width : 530px) {
    font-size: 13px;
    margin: 10px 0 0 30px;
  }

  @media (min-width : 790px) {
    font-size: 14px;
  }

  @media (min-width : 1050px) {
    font-size: 15px;
  }
`;

const Guider = () => {
  const [modal, setModal] = useState(false);

  const toggleModal = () => {
    modal === true ? setModal(false) : setModal(true)
  }

  return (
    <div>
      <GuideButton onClick={toggleModal}>
        {modal === true ? 'X' : '?'}
      </GuideButton>
      {modal &&
        <GuideModal>
          <MainSection>
            <SubSection>
              <img src={search} alt="search" height="25px" />
            </SubSection>
            <SubSection>
              <Title>유통기한/제품명/지역명으로<br />상품을 찾고 싶다면?</Title>
              <Text>
                예시) 유통기한 검색은 <b>2019.08</b><br />
                예시) 제품명 검색은 <b>들기름</b><br />
                예시) 지역명 검색은 <b>고창</b>
              </Text>
            </SubSection>
          </MainSection>
          <MainSection>
            <SubSection>
              <img src={qrcode} alt="qrcode" height="101px" />
            </SubSection>
            <SubSection>
              <Title>특정 상품에 대한 내용을<br />바로 알고 싶다면?</Title>
              <Text>상품 하단에 있는 재미난<br /><b>#키워드</b> 를 검색해보세요.</Text>
              <Text>예시) <b>푹신한 고래</b> 입력</Text>
            </SubSection>
          </MainSection>
        </GuideModal >}
    </div >
  );
};

export default Guider;