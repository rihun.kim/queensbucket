import React, { Component } from 'react';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import styled from "styled-components";

import {getScreen} from '../resources/cards/CardApi';
import {getMap} from '../resources/maps/MapApi';

const Viewer = styled.div`
  width: 100%;
  height: 100%;

  @media (min-width : 530px) {
    display: grid;  
    grid-gap: 10px;
    grid-template-columns: repeat(2, minmax(250px, 1fr));
  }

  @media (min-width : 790px) {
    grid-template-columns: repeat(3, minmax(250px, 1fr));
  }

  @media (min-width : 1050px) {
    grid-template-columns: repeat(4, minmax(250px, 1fr));
  }

  margin: 20px 0 0 0;
`;

const Card = styled.div`
  width: 100%;
  height: 630px;
  
  background-image: url(${({screen}) => getScreen(screen)});
  background-size: cover;
  background-position: center center;
  // border-radius: 15px;
  // box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.19), 0 1px 0 rgba(0, 0, 0, 0.23);
  position: relative;
`;

const Name = styled.div`
  color: #000000;
  font-size: 20px;
  font-weight: 600;
  margin: 20px 20px 0 20px;
`;

const EName = styled.div`
  color: #777777; 
  font-size: 12px;
  font-weight: 200;
  margin: 5px 20px 0 20px;
`;

const HDate = styled.div`
  color: #333333; 
  font-size: 12px;
  font-weight: 350;
  margin: 30px 20px 0 20px;
`;

const MDate = styled.div`
  color: #333333;  
  font-size: 12px;
  font-weight: 350;
  margin: 3px 20px 0 20px;
`;

const MProcess = styled.div`
  color: #333333; 
  font-size: 12px;
  font-weight: 350;
  margin: 3px 20px 0 20px;
`;

const Qc = styled.div`
  color: #333333; 
  font-size: 12px;
  font-weight: 350;
  margin: 3px 20px 0 20px;
`;

const Info = styled.div`
  color: #000000; 
  font-size: 13px;
  font-weight: 340;
  margin: 10px 20px 0 20px;
`;

const Map = styled.div`
  width: 270px;
  height: 287px;

  background-image: url(${({map}) => getMap(map)});
  background-size: cover;
  background-position: center center;
  margin: 30px auto 0 auto;
  position: relative;

  @media (min-width : 530px) {
    width: 210px;
    height: 222px;

    bottom: 120px;
    margin: 0 0 0 0;
    position: absolute;
    right: 18%;
  }

  @media (min-width : 790px) {
    width: 210px;
    height: 222px;

    bottom: 120px;
    margin: 0 0 0 0;
    position: absolute;
    right: 18%;
  }

  @media (min-width : 1050px) {
    width: 210px;
    height: 222px;

    bottom: 120px;
    margin: 0 0 0 0;
    position: absolute;
    right: 20px;
  }
`;

const Code = styled.div`
  color: #BBBBBB;
  font-size: 10px;
  font-weight: 350;

  bottom: 15px;
  position: absolute;
  margin: 0 0 0 15px;
`;

const Title = styled.div`
  color: #333333;  
  font-size: 13px;
  font-weight: 600;
  margin: 30px 20px 10px 20px;
`;

const Record = styled.div`
  color: #333333;  
  font-size: 13px;
  font-weight: 350;
  margin: 5px 20px 0 25px;
`;

export default class CardContainer extends Component {
  renderProduct = product => {
    return (
      <Carousel showStatus={false} showThumbs={false}>
        <Card key={product.code} screen={product.screen}>
          <Name>
            {product.name}
          </Name>
          <EName>
            {product.ename}
          </EName>
          <HDate>
            수확일자 : {product.hdate}
          </HDate>
          <MDate>
            제조일자 : {product.mdate}
          </MDate>
          <MProcess>
            제조방식 : {product.mprocess}
          </MProcess>
          <Qc>
            품질인증 : {product.qc}
          </Qc>
          <Info>
            {product.info}
          </Info>
          <Map map={product.map}>
          </Map>
          <Code>
            #{product.code}
          </Code>
        </Card>
        <Card key={product.code} screen={product.screen}>
          <Name>
            {product.name}
          </Name>
          <EName>
            {product.ename}
          </EName>
          <Title>
            [ 제품 특징 ]
          </Title>
          <Record>
            유질감 : {product.oil}
          </Record>
          <Record>
            고소함 : {product.savor}
          </Record>
          <Record>
            끝맛 : {product.finish}
          </Record>
          <Record>
            맛느낌 : {product.taste}
          </Record>
          <Record>
            발연점 : {product.stemp}
          </Record>
          <Record>
            레시피 : {product.recipe}
          </Record>
          <Title>
            [ 식품 이력 ]
          </Title>
          <Record>
            재배지역 : {product.area}
          </Record>
          <Record>
            재배농부 : {product.farmer}
          </Record>
          <Record>
            재배형태 : {product.place}
          </Record>
          <Record>
            수확일자 : {product.hdate}
          </Record>
          <Record>
            저장창고 : {product.ftemp}
          </Record>
          <Record>
            제조일자 : {product.mdate}
          </Record>
          <Record>
            착유최고온도 : {product.fhtemp}
          </Record>
        </Card>
      </Carousel>
    );
  }

  render() {
    return ( 
      <Viewer>
        {this.props.products.map(product => {
          return this.renderProduct(product);
        })}
      </Viewer>
    );  
  }
}
